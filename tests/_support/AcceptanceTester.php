<?php
use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
use Page\login;
use Page\dashboard;
use Page\contact;

class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

   /**
    * Define custom actions here
    */

    /**
  * @Given I am valid user to salesforce with valid credentials
  */
/*  public function iAmValidUserToSalesforceWithValidCredentials()
  {

  		$this->amOnPage(login::$URL);
  		$this->fillField(login::$usernameField, 'yogi250@gmail.com');
  		$this->fillField(login::$passwordField, 'testing123');
  		$this->click(login::$loginButton);
  		$this->waitForElement('#salesforceLogo',90);
  		$this->seeInCurrentUrl('one.app');
      //$this->print("Navigated to Browser");

    // throw new \Codeception\Exception\Incomplete("Step `I am valid user to salesforce with valid credentials` is not defined");
  }

 /**
  * @When I navigate to Home Page
  */
/*  public function iNavigateToHomePage()
  {

    $this->wait(05);
    $this->seeInCurrentUrl('one.app');
    $this->waitForElement(dashboard::$home, 90);
    $this->click('Home');
    $this->seeInCurrentUrl('/one.app#/home');
  //  $this->print("Navigate to HOME PAGE");
    // throw new \Codeception\Exception\Incomplete("Step `I navigate to Home Page` is not defined");
  }

 /**
  * @Then I navigate to Contacts Page
  */
/*  public function iNavigateToContactsPage()
  {
    //$this->wait(05);
    $this->seeInCurrentUrl('one.app');
    $this->waitForElement(dashboard::$contacts, 90);
    $this->click('Contacts');
    $this->seeInCurrentUrl('/Contact/');
  //  $this->print("Navigated to Contacts Page");
  //   throw new \Codeception\Exception\Incomplete("Step `I navigate to Contacts Page` is not defined");
}

  /**
 * @When below valid data table was given to create New Contact
 */
/* public function belowValidDataTableWasGivenToCreateNewContact(\Behat\Gherkin\Node\TableNode $table)
 {
  // $this->iNavigateToContactsPage();

  // $this->waitForElement(contact::$new, 90);
  $this->wait(05);
  $this->seeInCurrentUrl('one.app');
  $this->waitForElement(dashboard::$home, 90);
  $this->click('Home');
  $this->seeInCurrentUrl('/one.app#/home');

//  $this->seeInCurrentUrl('one.app');
  $this->waitForElement(dashboard::$contacts, 90);
  $this->click('Contacts');
  $this->seeInCurrentUrl('/Contact/');

  $this->wait(05);
  $this->click(contact::$new);
   $hash = $table->getHash();
        foreach ($hash as $keys=>$row) {
           if($keys>=0){

   //$this->waitForElement(contact::$createContactTitle,10);
   $this->waitForElement(contact::$firstName,10);
//   $this->selectOption(contact::$salutationDropdown,$row['Salutation']);
   $this->fillField(contact::$firstName,$row['First Name']);
   $this->fillField(contact::$lastName,$row['Last Name']);
  // $this->fillField(contact::$phone,$row['Phone']);
  // $this->fillField(contact::$HomePhone,$row['Home Phone'];
  // $this->fillField(contact::$accountName,$row['Account Name']);
  // $this->fillField(contact::$mobile,$row['Mobile']);
  // $this->fillField(contact::$title,$row['$title']);
  // $this->fillField(contact::$OtherPhone,$row['Other Phone'];
  // $this->fillField(contact::$department,$row['Department']);
  // $this->fillField(contact::$fax,$row['Fax']);
  // $this->fillField(contact::$BirthDate,$row['Birth Date']);
  // $this->fillField(contact::$eMail,$row['Email']);
  // $this->fillField(contact::$reportTo,$row['Reports To']);
  // $this->fillField(contact::$Assistant,$row['$Assistant']);
  // $this->selectOption(contact::$LeadSource,$row['Lead Source']);
  // $this->fillField(contact::$AsstPhone,$row['Assistant Phone']);
  // $this->fillField(contact::$MailingStreet,$row['Mailing Address']);
  // $this->fillField(contact::$MailingCity,$row['Mailing City']);
  // $this->fillField(contact::$MailingState,$row['Mailing State']);
  // $this->fillField(contact::$MailingZip,$row['Mailing Zip']);
  // $this->fillField(contact::$country,$row['Mailing Country']);
  // $this->fillField(contact::$OtherStreet,$row['Other Street']);
  // $this->fillField(contact::$OtherCity,$row['Other City']);
  // $this->fillField(contact::$OtherState,$row['Other State']);
  // $this->fillField(contact::$OtherZip,$row['Other Zip']);
  // $this->fillField(contact::$OtherCountry,$row['Other Country']);
  // $this->fillField(contact::$Languages,$row['Languages']);
//   $this->selectOption(contact::$Levels,$row['Levels']);
//   $this->fillField(contact::$Description,$row['Description']);
//   $this->wait(15);
             $this->click(contact::$save);
             $this->wait(02);
             $this->See('Your Contact Created');
                       //$this->See('Invalid Data');
               // $this->dontSee('Error No matches found');
               // $this->dontSee('Error You must enter a value');
               // $this->dontSee('The orders account must match the contracts account');

             $keys = $row;
             continue;





  //  throw new \Codeception\Exception\Incomplete("Step `below valid data table was given to create New Contact` is not defined");
      }
    }
 }


/**
 * @Then New Contact Should be created
 */
/* public function newContactShouldBeCreated()
 {
  //  throw new \Codeception\Exception\Incomplete("Step `New Contact Should be created` is not defined");
 }

}
*/
/**
     * @Given I am valid user to salesforce with valid credentials
     */
     public function iAmValidUserToSalesforceWithValidCredentials()
     {
       $this->amOnPage(login::$URL);
       $this->fillField(login::$usernameField, 'yogi250@gmail.com');
       $this->fillField(login::$passwordField, 'testing123');
       $this->click(login::$loginButton);
      // $this->waitForElement('#salesforceLogo',90);
       //$this->seeInCurrentUrl('one.app');
        //throw new \Codeception\Exception\Incomplete("Step `I am valid user to salesforce with valid credentials` is not defined");
     }

    /**
     * @When below valid data table was given to create New Contact
     */
   public function belowValidDataTableWasGivenToCreateNewContact(\Behat\Gherkin\Node\TableNode $table)
  {

    //  $this->wait(05);
    //  $this->See('Setup Home');
      $this->waitForElementVisible("//span[@data-aura-rendered-by='386:7;a']",30);
      $this->wait(10);
      $this->See('Setup Home');
      $this->wait(05);
      $this->click(login::$Contacts);
      $this->See('Recently Viewed');
      $this->waitForElementVisible("//span[contains(.,'Contact Owner')]",30);
      $this->cLick(login::$new);
      $this->wait(05);
      $this->See('Create Contact');
      $this->wait(05);
      //$this->wait(05);
      //$this->click(login::$new);
     // $this->wait(05);
    //  $this->See('Create Contact');
        $hash = $table->getHash();
             foreach ($hash as $keys=>$row) {
                if($keys>=0){
                 // $this->wait(05);
                //  $this->click(login::$new);

        //  $this->selectOption(contact::$salutationDropdown,$row['Salutation']);
      //    $this->fillField(login::$firstName,$row['First Name']);
        //   $this->wait(05);
      //  $this->fillField(contact::$lastName,$row['Last Name']);
       // $this->fillField(contact::$phone,$row['Phone']);
       // $this->fillField(contact::$HomePhone,$row['Home Phone'];
       // $this->fillField(contact::$accountName,$row['Account Name']);
       // $this->fillField(contact::$mobile,$row['Mobile']);
       // $this->fillField(contact::$title,$row['$title']);
       // $this->fillField(contact::$OtherPhone,$row['Other Phone'];
       // $this->fillField(contact::$department,$row['Department']);
       // $this->fillField(contact::$fax,$row['Fax']);
       // $this->fillField(contact::$BirthDate,$row['Birth Date']);
       // $this->fillField(contact::$eMail,$row['Email']);
       // $this->fillField(contact::$reportTo,$row['Reports To']);
       // $this->fillField(contact::$Assistant,$row['$Assistant']);
       // $this->selectOption(contact::$LeadSource,$row['Lead Source']);
       // $this->fillField(contact::$AsstPhone,$row['Assistant Phone']);
       // $this->fillField(contact::$MailingStreet,$row['Mailing Address']);
       // $this->fillField(contact::$MailingCity,$row['Mailing City']);
       // $this->fillField(contact::$MailingState,$row['Mailing State']);
       // $this->fillField(contact::$MailingZip,$row['Mailing Zip']);
       // $this->fillField(contact::$country,$row['Mailing Country']);
       // $this->fillField(contact::$OtherStreet,$row['Other Street']);
       // $this->fillField(contact::$OtherCity,$row['Other City']);
       // $this->fillField(contact::$OtherState,$row['Other State']);
       // $this->fillField(contact::$OtherZip,$row['Other Zip']);
       // $this->fillField(contact::$OtherCountry,$row['Other Country']);
       // $this->fillField(contact::$Languages,$row['Languages']);
     //   $this->selectOption(contact::$Levels,$row['Levels']);
     //   $this->fillField(contact::$Description,$row['Description']);
     //   $this->wait(15);
                //  $this->click(contact::$save);
                //  $this->wait(02);
                //  $this->See('Your Contact Created');
                            //$this->See('Invalid Data');
                    // $this->dontSee('Error No matches found');
                    // $this->dontSee('Error You must enter a value');
                    // $this->dontSee('The orders account must match the contracts account');

                  $keys = $row;
                  continue;





        //throw new \Codeception\Exception\Incomplete("Step `below valid data table was given to create New Contact` is not defined");
     }
   }
 }

    /**
     * @Then New Contact Should be created
     */
     public function newContactShouldBeCreated()
     {
        //throw new \Codeception\Exception\Incomplete("Step `New Contact Should be created` is not defined");
     }
}
