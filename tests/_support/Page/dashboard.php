<?php
namespace Page;

class dashboard
{
    // include url of current page
//    public static $home = 'Home';
    public static $home = "//a[@href='#/home']";
    public static $opportunities = "//a[@href='#/sObject/Opportunity/home']";
    public static $leads = "//a[@href='#/sObject/Lead/home']";
    public static $tasks = "//a[@href='#/sObject/Task/home']";
    public static $files = "//a[@href='#/sObject/ContentDocument/home']";
    public static $notes = "//a[@href='#/sObject/ContentNote/home']";
    public static $accounts = 'html/body/div[6]/div[1]/div/div/div/div/ul/li[7]/a/div/div[1]/div/div/span/img';
    public static $contacts = "//a[@href='#/sObject/Contact/home']";
    public static $campaigns = 'html/body/div[6]/div[1]/div/div/div/div/ul/li[9]/a/div/div[1]/div/div/span/img';
    public static $dashboards = 'html/body/div[6]/div[1]/div/div/div/div/ul/li[10]/a/div/div[1]/div/div/span/img';
    public static $reports = 'html/body/div[6]/div[1]/div/div/div/div/ul/li[11]/a/div/div[1]/div/div/span/img';
    public static $feed = 'html/body/div[6]/div[1]/div/div/div/div/ul/li[12]/a/div/div[1]/div/div/span/img';
    public static $groups = 'html/body/div[6]/div[1]/div/div/div/div/ul/li[13]/a/div/div[1]/div/div/span/img';
    public static $calendar = 'html/body/div[6]/div[1]/div/div/div/div/ul/li[14]/a/div/div[1]/div/div/span/img';
    public static $people = 'html/body/div[6]/div[1]/div/div/div/div/ul/li[15]/a/div/div[1]/div/div/span/img';
    public static $cases = 'html/body/div[6]/div[1]/div/div/div/div/ul/li[16]/a/div/div[1]/div/div/span/img';
    public static $searchSalesforce = 'html/body/div[6]/div[1]/header/div[3]/div[2]/div/div/div/div[1]/input';
    public static $globalActions = 'html/body/div[6]/div[1]/header/div[3]/div[3]/div[1]/div/div[1]/div/div/a/div';
    public static $appLauncher = 'html/body/div[6]/div[1]/header/div[3]/div[3]/a[1]/div';
    public static $helpAndTraining = 'html/body/div[6]/div[1]/header/div[3]/div[3]/div[2]/div/div/div[1]/div/div/a/div';
    public static $setup = 'html/body/div[6]/div[1]/header/div[3]/div[3]/div[3]/div/div[1]/div/div/a/div/span[1]/span/span[4]';
    public static $notifications = 'html/body/div[6]/div[1]/header/div[3]/div[3]/a[2]';
    public static $viewProfile = '//img[@alt="View Profile"]';
    public static $logOut = '//a[contains(.,"Log Out")]';
	
    
    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }


}
