<?php
namespace Page;

class contact
{
    // include url of current page
    public static $recentlyViewedDropdown = '';
    public static $listViewControls = '';
    public static $refresh = '';
    public static $Contacts = "//span[@data-aura-rendered-by='386:7;a']";
    public static $new = "//a[@data-aura-rendered-by='18:333;a']";
    public static $import = '';
    public static $form="//form[contains(@class, 'forceDetailPanel')]";
    // variables for new contact
    public static $createContactTitle= "//h2[contains(.,'Create Contact')]";
    public static $salutationDropdown = "//select[@id='8348:1;0']";
    public static $firstName = "//input[@placeholder='First Name']";
    public static $middleName = "//input[@placeholder='Middle Name']";
    public static $lastName = "//input[@placeholder='Last Name']";
    public static $suffix = "//input[@placeholder='Suffix']";
    //account should exist before selecting account name
    public static $accountName = "//input[@placeholder='Select Account']";
    public static $title = '//label[contains(., "Title")]/following::input[1]';
    public static $eMail = "//input[@type='email']";
    public static $phone = "//input[@id='34:3649;a']";
    public static $HomePhone = "//input[@id='185:3649;a']";
    public static $mobile = '//label[contains(., "Mobile")]/following::input[1]';
    public static $OtherPhone = "//input[@id='469:3649;a']";
    public static $reportTo = "//input[@placeholder='Select Contact']";
    public static $Assistant = "//input[@id='1170:3649;a']";
    public static $LeadSource = "//a[@aria-label='Lead Source']";
    public static $AsstPhone = "//input[@id='1267:3649;a']";
    public static $department = '//label[contains(., "Department")]/following::input[1]';
    public static $fax = '//label[contains(., "Fax")]/following::input[1]';
    public static $BirthDate = "//input[@id='570:3649;a']";
    //public static $eMail = "//input[@id='952:3649;a']";
    public static $MailingStreet = "//textarea[@id='1347:3649;a']";
    public static $OtherStreet = "//textarea[@id='1469:3649;a']";
    public static $MailingCity = "//input[@id='1365:3649;a']"
    public static $MailingState = "//input[@id='1326:3649;a']";
    public static $MailingZip = "//input[@id='1385:3649;a']";
    public static $country = "//input[@id='1310:3649;a']";

    public static $OtherStreet = "//textarea[@id='1469:3649;a']";
    public static $OtherCity = "//input[@id='1487:3649;a']";
    public static $OtherState = "//input[@id='1448:3649;a']";
    public static $OtherZip = "//input[@id='1507:3649;a']";
    public static $OtherCountry = "//input[@id='1432:3649;a']";
    public static $Languages = "//input[@id='1564:3649;a']";
    public static $Levels = "//a[@aria-label='Level']";
    public static $Description = "//textarea[@id='1743:3649;a']";

    public static $cancel = '';
    public static $save = "//button[@title='Save']";
    public static $closeTheWindow = '';
    public static $contactName = "//h3[text()='Contact']/following::h1/span";



    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }


}
