<?php
namespace Page;

class login
{
    // include url of current page
    public static $URL = '/';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */
	public static $usernameField = '#username';
	public static $passwordField = '#password';
	public static $loginButton = '#Login';
	public static $logo = '#logo';
  public static $Contacts = "//span[@data-aura-rendered-by='386:7;a']";
  public static $new = "//div[contains(.,'New')]";
  public static $createContact = "//div[contains(@title,'New')]";
  public static $firstName = "//input[@placeholder='First Name']";
	//public static $loginPageName = 'site_login.php';
    /**

     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }


}
