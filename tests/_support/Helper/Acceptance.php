<?php
namespace Helper;

use Page\login;
use Page\dashboard;
use Page\contact;

class Acceptance extends \Codeception\Module
{
//Dashboard functions
	public function defaultLogin($I){
		$I->amOnPage(login::$URL);
		$I->fillField(login::$usernameField, 'yogi250@gmail.com');
		$I->fillField(login::$passwordField, 'testing123');
		$I->click(login::$loginButton);
		$I->waitForElement('#salesforceLogo',90);
		$I->seeInCurrentUrl('one.app');
		
	}
	public function loginWithValues($I,$name,$password){

		$I->amOnPage(login::$URL);
		$I->fillField(login::$usernameField, $name);
		$I->fillField(login::$passwordField, $password);
		$I->click(login::$loginButton);
		$I->waitForElement('#salesforceLogo',90);
		$I->seeInCurrentUrl('one.app');
	
	}
	
	public function logOut($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$viewProfile,15);
		$I->click(dashboard::$viewProfile);
		$I->click(dashboard::$logOut);
		$I->waitForElement(login::$logo);
		$I->dontSeeInCurrentUrl('one.app');
	
	}

	public function goToHome($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$home, 90);
		$I->click('Home');
		$I->seeInCurrentUrl('/one.app#/home');
		
	}
	
	public function goToOpportunities($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$opportunities, 90);
//		$I->click(dashboard::$opportunities);
		$I->click('Opportunities');
		$I->seeInCurrentUrl('/Opportunity/');
	}
	
	public function goToLeads($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$leads, 90);
		$I->click('Leads');
		$I->seeInCurrentUrl('/Lead/');
	}

	public function goToTasks($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$tasks, 90);
		$I->click('Tasks');
		$I->seeInCurrentUrl('/Task/');
	}
	public function goToFiles($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$files, 90);
		$I->click('Files');
		$I->seeInCurrentUrl('/ContentDocument/');
	}
	public function goToNotes($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$notes, 90);
		$I->click('Notes');
		$I->seeInCurrentUrl('/ContentNote/');
	}
	public function goToAccounts($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$accounts, 90);
		$I->click('Accounts');
		$I->seeInCurrentUrl('/Account/');
		$I->seeElement('.uiOutputText', ['text' => 'Accounts']);
	}
	public function goToContacts($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$contacts, 90);
		$I->click('Contacts');
		$I->seeInCurrentUrl('/Contact/');
	}
	public function goToCampaigns($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$campaigns, 90);
		$I->click('Campaigns');
		$I->seeInCurrentUrl('/Campaign/');
	}
	public function goToDashboards($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$dashboards, 90);
		$I->click('Dashboards');
		$I->seeInCurrentUrl('/Dashboard/');
	}
	public function goToReports($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$reports, 90);
		$I->click('Reports');
		$I->seeInCurrentUrl('/Report/');
	}
	public function goToGroups($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$groups, 90);
		$I->click('Groups');
		$I->seeInCurrentUrl('/CollaborationGroup/');
	}
	public function goToCalender($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$calendar, 90);
		$I->click('Calendar');
		$I->seeInCurrentUrl('/Event/');
	}
	public function goToFeed($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$feed, 90);
		$I->click('Feed');
		$I->seeInCurrentUrl('//');
	}
	public function goToPeople($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$people, 90);
		$I->click('People');
		$I->seeInCurrentUrl('/User/');
	}
	public function goToCases($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$cases, 90);
		$I->click('Cases');
		$I->seeInCurrentUrl('/Case/');
	}
	public function clickOnAppLauncher($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$appLauncher, 90);
		$I->click('App Launcher');
		$I->seeInCurrentUrl('/UserAppMenuItem/');
	}
	public function clickGlobalActions($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$globalActions, 90);
		$I->click('Global Actions');

	}
	public function clickOnHelpAndTraining($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$helpAndTraining, 90);
		$I->click(dashboard::$helpAndTraining);
		$I->waitForText('Help and Training', 30);
	}
	public function searchSalesforceWith($searchText){
		$I = $this;
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$searchSalesforce, 90);

	}
	public function clickOnSetup($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$setup, 90);
		$I->click(dashboard::$setup);
		$I->waitForText('SETUP', 30);
		
	}
	public function clickOnNotifications($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$notifications, 90);
		$I->click('Notifications');
		
	}
	public function clickOnViewProfile($I){
		$I->seeInCurrentUrl('one.app');
		$I->waitForElement(dashboard::$viewProfile, 90);
		$I->click('View Profile');
		
	}
//create a new contact
	public function createNewContact($I){
		
		
		
		$I->goToContacts($I);

		$I->waitForElement(contact::$new, 90);
		$I->click(contact::$new);
		$I->waitForElement(contact::$createContactTitle,10);
		$I->waitForElement(contact::$firstName,10);
		$I->fillField(contact::$firstName,'Mudit1');
	//	$I->fillField(contact::$middleName,'');
		$I->fillField(contact::$lastName,'Choudhary');
		/* $I->fillField(contact::$suffix,'Mr.');
		$I->click(contact::$accountName);
		//$I->click('accountname');
		$I->fillField(contact::$accountName, 'accountname');
		$I->waitForElement('//mark[text()="accountname"]', 30);
		$I->click('//mark[text()="accountname"]');
	//	$I->selectOption(contact::$accountName,'accountname');
	//	$I->selectOption(contact::$reportTo,'lastname');
		$I->fillField(contact::$title,'Test Engineer');
 */		$I->fillField(contact::$department,'QA');
		$I->fillField(contact::$eMail,'demotest@gmail.com');
		$I->fillField(contact::$phone,'0122456789');
		$I->fillField(contact::$mobile,'099118822');
		//$I->fillField(contact::$fax,'');
		//$I->fillField(contact::$street,'xyz street');
		/* $I->fillField(contact::$city,'auckland');
		$I->fillField(contact::$state,'auckland');
		$I->fillField(contact::$zip,'1041');
		$I->fillField(contact::$country,'NZ');
		$I->fillField(contact::$lastName,'Choudhary'); */
		$I->click(contact::$save);
	//	$I->waitForElement(contact::$contactName,10);
		$I->see('Mudit1 Choudhary');
		$I->wait(5);
		
		
	}
	
	public function editContact($I){

		$I->waitForElement(contact::$new, 90);

		$I->seeNumberOfElements('//tbody/tr/th/a',4);
		$els = $I->grabMultiple('//tbody/tr/th/a','title');

 		foreach ((array)$els as $value) {
 			codecept_debug($value);
		
 		}
// 		$I->wait(5);
	}
}
