<?php


class createContactCest
{
    public function _before(AcceptanceTester $I)
    {
		$I->maximizeWindow();
		
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function login(AcceptanceTester $I)
    {
    	$I->defaultLogin($I);
    }
    public function newCotact(AcceptanceTester $I)
    {
    	$I->createNewContact($I);
    }
    public function editCotact(AcceptanceTester $I)
    {
    	$I->editContact($I);
    }
    public function deleteCotact(AcceptanceTester $I)
    {
    	$I->deleteContact($I);
    }
}
