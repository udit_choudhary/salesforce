Feature: Create New Contact

  In order to add new contact
  As a salesforce user
  I need to have a valid data

  Scenario: login to salesforce before creating new Order
  Given I am valid user to salesforce with valid credentials

  Scenario: try creating New Contact with valid data
    Given I am valid user to salesforce with valid credentials
    When below valid data table was given to create New Contact

        ##Notes:
            #There are 2 specific Conditions to create New Contact
            #First Name and Last Name Must not be NULL(Means field must be specified)

  |TCID |Salutation |First Name |Last Name  |Phone     |Home Phone  |Account Name |Mobile     |Title   |Other Phone |Department |Fax      |Birth Date |Email                 |Reports To        |Lead Source |Assistant Phone |Mailing Address |Mailing City |Mailing State |Mailing Zip |Mailing Country |Other Street |Other City |Other State |Other Zip |Other Country |Languages |Levels |Description   |
  |TC01 |Mr         |Russell    |Murphy     |095257928 |095292970   |GetSkills    |0226106991 |Manager |096106991   |PURCHASE   |09769387 |20/10/1975 |getskills@yahoo.com   |Mudit1 Choudhary  |Web         |9848484         |Ewenson Avenue  |Auckland     |NZ            |1061        |New Zealand     |Victoria     |Wellington |New Zealand |1071      |New Zealand   |English   |01     |Urgent Request|

   Then New Contact Should be created
